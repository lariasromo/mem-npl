#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyttsx
import io
import requests
import json
import pprint
import json
import glob
import csv
import wave
import pyaudio
import os
import subprocess
import speech_recognition as sr
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

def transcribe_file(speech_file, file_name):
    """Transcribe the given audio file."""
    client = speech.SpeechClient()

    with io.open(speech_file, 'rb') as audio_file:
        content = audio_file.read()

    audio = types.RecognitionAudio(content=content)
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=44100,
        language_code='es-MX')

    response = client.recognize(config, audio)

    textFile = ""
    fileContent = open('audios_transcripts/'+file_name+'.txt', 'w')
    for index, result in enumerate(response.results):
        print("Resultado: " + str(index+1))
        responseIndex = result.alternatives[0].transcript.encode('utf-8', 'strict')
        print(responseIndex)
        print("")
        fileContent.write("\nResultado: " + str(index+1) + "\n")
        fileContent.write(str(responseIndex))
    fileContent.close()

def mostSimilar(question):
    csvFile = "C:\\Users\\luisaria\\Documents\\school\\speech_recognition_python\\outputRM.csv"
    csvIDList = "C:\\Users\\luisaria\\Documents\\school\\speech_recognition_python\\outputIDS.csv"

    maxSim = 0
    secondID = 0
    question_id = 0
    with open(csvFile, 'r') as csvfile, open(csvIDList, 'r') as idList:
        spamreader = csv.DictReader(csvfile)
        ids = csv.DictReader(idList)
        for row in ids:
            if str(question) in row['metadata_file']:
                question_id = row['id']
                break

        for row in spamreader:
            if question_id == row['FIRST_ID']:
                maxSim = row['SIMILARITY']
                firstID = row['FIRST_ID']        
                secondID = row['SECOND_ID']
                print(firstID)
                print( secondID)
                # print( maxSim )
                break

    with open(csvIDList, 'r') as idList:
        ids = csv.DictReader(idList)
        for row in ids:
            if secondID == row['id']:
                print("Entre en last for" + str(row['metadata_file'].split('.')[0]))
                return row['metadata_file'].split('.')[0]

        return None


def runRM():    
    command = ['C:\\Program Files\\RapidMiner\\RapidMiner Studio\\scripts\\rapidminer-batch.bat', '//MEM/mem-npl'] 
    p = subprocess.Popen(command)
    p.communicate() 

def convert_audios(audio_file, output_file):    
    command = ['C:\\Program Files (x86)\\sox-14-4-2\\sox', audio_file, '-c', '1', output_file] 
    p = subprocess.Popen(command)
    p.communicate()

def play_file(file_name):    
    chunk = 1024   
    # open the file for reading.
    wf = wave.open(file_name, 'rb')
    # create an audio object
    p = pyaudio.PyAudio()
    # open stream based on the wave object which has been input.
    stream = p.open(format =
                    p.get_format_from_width(wf.getsampwidth()),
                    channels = wf.getnchannels(),
                    rate = wf.getframerate(),
                    output = True)
    # read data (based on the chunk size)
    data = wf.readframes(chunk)
    # play stream (looping from beginning of file to the end)
    while data != '':
        # writing to the stream is what *actually* plays the sound.
        stream.write(data)
        data = wf.readframes(chunk)
    # cleanup stuff.
    stream.close()    
    p.terminate()

def talk(text_to_speak):
    engine = pyttsx.init()
    engine.say(text_to_speak)
    engine.runAndWait()

def listen(question_to_ask):
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Ask something!")
        audio = r.listen(source)        
    try:
        question = r.recognize_google(audio, language="es-MX")
        print("You asked: " + question)
        fileContent = open('question_bank/'+str(question_to_ask)+'.txt', 'w')
        fileContent.write(question)
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
    return question


if __name__ == '__main__':    
    # talk("Hola adrian como estas")
    talk("Good morning, please say your question..")
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "C:\\Users\\luisaria\\Documents\\school\\speech_recognition_python\\API Project-53db09d5f6a2.json"
    DIR="question_bank"
    question_to_ask = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))]) + 1
    question_text = listen(question_to_ask)
    print("You asked " + question_text)
    talk("You asked " + question_text)
    talk("Thinking on your answer..")
    runRM()
    similarities = mostSimilar(question_to_ask)
    talk("The answer for your question is")
    play_file("answer_bank/"+str(similarities)+'.wav')
    os.remove("./question_bank/"+question_to_ask+'.txt')